# Traversability Segmentation
Color changing based off of https://python-forum.io/Thread-Change-color-pixel-in-an-image

I have used data from a histogram of this image in the Rocky Mountains of Utah for hue and value with bins=10 to find 5 places 
with the greatest number of pixels at a certain hue value and value value.
Using that data I changed the RGB of those hue and value ranges to a value different value for each of the ranges and one another for 
the less common hue and value values in this type of image.  I did not include saturation since the segmentation data presented
in its image is not useful for my traversability project.
This is not perfect since the hue of the lake is the same as that on top of a mountain but the audience is able to gather terrain info from the 
segmentation.
