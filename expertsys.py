from PIL import Image
import matplotlib.pyplot as plt
from matplotlib import colors
from skimage import img_as_float
import numpy as np
import matplotlib.patches as mpatches

PICTURE = Image.open('utahwithoutlabels.jpg')
#   Using two different variables for hue and value
RGB = img_as_float(PICTURE)  # need image to be float to run without error
PIC = img_as_float(PICTURE)
# Get the size of the image
width, height = PICTURE.size

#   Change to HSV since histograms show starker contrast in HSV than RGB, gray
HSV = colors.rgb_to_hsv(PICTURE)
VAL = HSV[:, :, 2]   # Split attributes
HUE = HSV[:, :, 0]
counts, bin_edges = np.histogram(VAL, bins=10)
counts1, bin_edges1 = np.histogram(HUE, bins=10)
# Process every pixel, changing five peaks of HUE histogram to one hue value for each
for x in range(0, width):
    for y in range(0, height):
        r, g, b = RGB[y, x]     # have to switch order of coordinates to avoid index error
    #   values of value are mostly evenly spread out over all bins
        if HSV[y, x, 2] <= bin_edges[2]:   # using HSV data to change RGB image to be able to see segments
            RGB[y, x] = (0, 0, 0)  # black
        elif HSV[y, x, 2] <= bin_edges[4]:
            RGB[y, x] = (0.642, -0.219, -0.462)  # red
        elif HSV[y, x, 2] <= bin_edges[6]:
            RGB[y, x] = (0.405, 0.520, 0.158)  # green
        elif HSV[y, x, 2] <= bin_edges[8]:
            RGB[y, x] = (0.704, 0.395, -0.150)  # orange
        else:
            RGB[y, x] = (255, 255, 255)  # white

print('for loop for value done')

for x in range(0, width):
    for y in range(0, height):
        r, g, b = PIC[y, x]     # have to switch order of coordinates to avoid index error
    #   Hue has a lot more pixels at lower hue values so I need more colors to represent lower values
        if HSV[y, x, 0] <= bin_edges1[1]:   # using HSV data to change RGB image to be able to see segments
            PIC[y, x] = (0.642, -0.219, -0.462)     # red
        elif HSV[y, x, 0] <= bin_edges1[2]:
            PIC[y, x] = (0.704, 0.395, -0.150)  # orange
        elif HSV[y, x, 0] <= bin_edges1[3]:
            PIC[y, x] = (0.405, 0.520, 0.158)  # bright green
        elif HSV[y, x, 0] <= bin_edges1[4]:
            PIC[y, x] = (255, 255, 255)   # white
            # for i in range(0, 80):
            #     for b in range(x - i, x - i):
            #         PIC[y, x] = (0.165, -0.266, 0.294)
        elif HSV[y, x, 0] <= bin_edges1[5]:
            PIC[y, x] = (0, 0, 0)  # black
        else:
            PIC[y, x] = (0.460, 0.276, 0.444)  # purple

print('for loop for hue done')

plt.subplots(1, 3)

plt.subplot(1, 3, 1)
plt.imshow(PICTURE)
plt.axis('off')
plt.title('Original Image')

plt.subplot(1, 3, 2)
plt.imshow(RGB)
red_patch = mpatches.Patch(color='red', fill=True,
                           label='Value from {} to {}'.format(bin_edges[3], bin_edges[4]))
white_patch = mpatches.Patch(color='white', fill=True,
                             label='Value from {} to {}'.format(bin_edges[9], bin_edges[10]))
black_patch = mpatches.Patch(color='black', fill=True,
                             label='Value from {} to {}'.format(bin_edges[0], bin_edges[2]))
green_patch = mpatches.Patch(color='green', fill=True,
                             label='Value from {} to {}'.format(bin_edges[5], bin_edges[6]))
orange_patch = mpatches.Patch(color='orange', fill=True,
                              label='Value from {} to {}'.format(bin_edges[7], bin_edges[8]))
plt.legend(bbox_to_anchor=(0, 1, 1.02, 0.2), loc="lower left", title='Segmentation based on Value',
           ncol=2, fancybox=True, shadow=True, handles=[black_patch, red_patch, green_patch, orange_patch, white_patch])
plt.axis('off')


plt.subplot(1, 3, 3)
plt.imshow(PIC)
red_patch1 = mpatches.Patch(color='red', fill=True,
                            label='Hue from {} to {}'.format('%.3f' % bin_edges1[0], '%.3f' % bin_edges1[1]))
white_patch1 = mpatches.Patch(color='white', fill=True,
                              label='Hue from {} to {}'.format('%.3f' % bin_edges1[3], '%.3f' % bin_edges1[4]))
black_patch1 = mpatches.Patch(color='black', fill=True,
                              label='Hue from {} to {}'.format('%.3f' % bin_edges1[4], '%.3f' % bin_edges1[5]))
green_patch1 = mpatches.Patch(color='green', fill=True,
                              label='Hue from {} to {}'.format('%.3f' % bin_edges1[2], '%.3f' % bin_edges1[3]))
orange_patch1 = mpatches.Patch(color='orange', fill=True,
                               label='Hue from {} to {}'.format('%.3f' % bin_edges1[1], '%.3f' % bin_edges1[2]))
purple_patch1 = mpatches.Patch(color='purple', fill=True,
                               label='Hue from {} to {}'.format('%.3f' % bin_edges1[5], '%.3f' % bin_edges1[10]))
plt.legend(bbox_to_anchor=(0, 1, 1.02, 0.2), loc="lower left", title='Segmentation based on Hue',
           ncol=2, fancybox=True, shadow=True,
           handles=[red_patch1, orange_patch1, green_patch1, white_patch1, black_patch1, purple_patch1])
plt.axis('off')

plt.show()
